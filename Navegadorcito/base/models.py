from django.db import models

# Create your models here.

class consola(models.Model):
    nombre = models.CharField(max_length=255)
    version = models.CharField(max_length=255)
    modelo = models.CharField(max_length=255)
    marca = models.CharField(max_length=255)

    def __unicode__(self):  #lo que retorna la clase
        return"[%s/%s/%s/%s] %s" % (self.nombre,self.modelo,self.version,self.marca)


class rooms(models.Model):
    nombre = models.CharField(max_length=255)
    genero = models.CharField(max_length=255)
    desarrolladora = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)

    def __unicode__(self):  #lo que retorna la clase
        return"[%s/%s/%s/%s]" % (self.nombre,self.genero,self.desarrolladora,self.descripcion)

class usuario(models.Model):
    nombre = models.CharField(max_length=255)
    clave = models.CharField(max_length=15)
    id_consola = models.ForeignKey(consola, on_delete=models.CASCADE)
    id_room = models.ForeignKey(rooms, on_delete=models.CASCADE)

    def __unicode__(self):
        return "[%s/%s]" % (self.nombre,self.clave)